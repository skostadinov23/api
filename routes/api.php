<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * CRUD
 */
Route::post( '/posts/store', 'PostsApiController@store' );

Route::get( '/posts/{search?}', 'PostsApiController@index' );

Route::get( '/post/{id}', 'PostsApiController@show' );
Route::post( '/post/{id}/destroy', 'PostsApiController@destroy' );
Route::post( '/post/{id}/update', 'PostsApiController@update' );