<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;
use Validator;

class PostsApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( $search = null )
    {
        if( $search != null ){
            $posts = Posts::where( 'title', 'like', "%{$search}%" )->paginate( 2 );
            return $posts;
        }

        $posts = Posts::paginate( 2 );
        return $posts;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:posts|max:255',
            'except' => 'max:255',
            'content' => 'required',
            'author' => 'required|max:50',
            'email' => 'required|email:rfc',
            'publish_date' => 'required|date|date_format:Y-m-d',
        ]);

        $errors = [];
        if ( $validator->fails() ) {

            foreach ( $validator->messages()->getMessages() as $field_name => $messages ) {
                $errors[] = $messages;
            }

            return $errors;
        }


        $post = new Posts;

        $post->title = $request->title;
        $post->except = $request->except ?? "";
        $post->content = $request->content;
        $post->author = $request->author;
        $post->email = $request->email;
        $post->create_date = date( "Y-m-d" ,time() );
        $post->publish_date = $request->publish_date;

        $post->save();

        return [ "message" => "Successfuly stored" ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Posts::find( $id );

        if( $post ){
            return $post;
        }

        return [ "message" => "Not exist" ];
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request )
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:posts|max:255',
            'except' => 'max:255',
            'content' => 'required',
            'author' => 'required|max:50',
            'email' => 'required|email:rfc',
            'publish_date' => 'required|date|date_format:Y-m-d',
        ]);

        $errors = [];
        if ( $validator->fails() ) {

            foreach ( $validator->messages()->getMessages() as $field_name => $messages ) {
                $errors[] = $messages;
            }

            return $errors;
        }

        $post = Posts::find( $id );

        $post->title = $request->title;
        $post->except = $request->except;
        $post->content = $request->content;
        $post->author = $request->author;
        $post->email = $request->email;
        $post->publish_date = $request->publish_date;

        $post->save();

        return [ "message" => "Successfuly updated" ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Posts::destroy( $id );

        if( $destroy ){
            return [ "message" => "Successfuly destroyed" ];
        }

        return [ "message" => "error" ];
    }
}
